import './App.css';
import React, { useEffect, useState } from 'react';

// React maneja todo como componentes, cada componente es una clase o una funcion que devuelve un fragmento JSX a compilar. JSX es el lenguaje que utiliza React para compilar el frontend.

// React como clase puede ser creado de la siguiente manera:
// Se crea una clase que extienda a React.Component
class ReactClass extends React.Component {

  // Para definir valores por defecto se especifica un constructor que recibe una variable props que almacena las propiedades que se pasen al componente
  // en este caso se pasa "defaultCount" y si no llegar se ubica 0
  constructor(props) {
    super(props);
    // React maneja un estado "this.state" para el componente en el cual se puede almacena informacion.
    this.state = {count: this.props.defaultCount || 0};
  }

  // Se pueden definir metodos de utilidad
  add = () => {
    // Se utiliza setState para cambiar el estado, en este caso se le suma 1 al valor ya almacenado
    this.setState({count: this.state.count + 1})
  }

  // Se necesita agregar el metodo render que devuelva el JSX
  render() {
    // Como se puede observar la etiqueta <div> se esta usando fuera de un archivo html esto es gracias a JSX
    return (
      // La expresion <></> se usa como un <div> invisible, esto porque un componente React no puede tener como elemento principal mas de un elemento, en este caso un div y un button
      // Para mostrar un valor dentro de JSX o asignar algun valor a una propiedad se utilizan llaves {}.
      // Dentro del div mostramos el contador
      // A la propiedad onClick del boton le agregamos el metodo add
      <>
        <div>Componente React como clase: {this.state.count}</div>
        <button onClick={this.add}>Agregar</button>
      </>
    )
  }
}

// Sin embargo RECOMIENDO utilizar React como funciones (Programacion funcional) ya que es mucho mas entendible y practico 
// Considerar que en versiones antiguas de React no es posible utilizar funciones, es buena conocer las clases en caso de que se requiera.

// El mismo componente de React como funcion quedaria asi:
// Tambien puede ser: funcion ReactFunction () {}
// Es este caso estoy deconstruyendo props como { defaultCount } para que sea mas facil pero se puede recibir props y dentro del componente llamar a
// las propiedades desde esta constante props, por ejemplo "props.defaultCount"
// Aqui agrego "children" como propiedad, esta propiedad esta reservada para todo lo que vaya dentro de las etiquetas del componente.
const ReactFunction = ({ children, defaultCount }) => {
  // Los estados se definen de forma individual utilizando Hooks (Utilizados por react para poder trabajar sin definir una clase)
  // Los Hooks siempre empiezan con el prefijo "use", en este caso useState permite controlar el estado del componente
  // Aqui se define el mismo estado que antes, y obtenemos "count" que almacena el valor y "setCount" que permite cambiarlo
  // Los nombres de esta variable y funcion se pueden establecer como sea pero es convencion utilizar una palabra que defina lo que se almacena
  // y para la funcion de cambio utilizar el prefijo "set"
  const [count, setCount] = useState(defaultCount || 0)

  // La funcion de agregar es bastante parecida pero utilizando setCount. Notar que aqui como no es una clase no se utiliza la expresion "this"
  const add = () => {
    setCount(count + 1)
  }

  // useEffect permite ejecutar codigo en base a triggers (Cambios en el estado), este recibe como primer parametro la funcion a ejecutar y como 
  // segundo parametro la lista de campos que disparan el efecto, si la lista esta vacia [] se ejecutara solo una vez, si no se especifica una lista se 
  // llamara siempre que se haga cualquier cambio en el estado.
  // En este caso se lanza una alerta con el conteo cada vez que el valor cambia, por eso se agrega count a la lista de dependencias
  useEffect(() => {
    alert(count)
  }, [count])

  // La funcion debe devolver el JSX
  return (
    // El JSX no varia mucho, siemplemente se cambia la forma en que se llaman los valores del estadovo funciones.
    // Aqui se muestra tambien children.
    <>
      <div>Componente React como funcion: {count}</div>
      <button onClick={add}>Agregar</button>
      {children}
    </>
  )

}

// En lo personal considero que utilizar funciones es mucho mas comodo, solo hay que acostumbrarse al uso de hooks.

function App() {
  return (
    // Cada componente creado en React puede ser llamado como si fuera una etiqueta html pasando las propiedades definidas para cada uno.
    // En este caso se puede obervar que cada componente tiene un estado propio, cada uno maneja su contador sin afectar al otro.
    // El componente ReactFuncion tiene dentro un texto, este es pasado como la propiedad children del componente. Se puede jugar con esto para anidar componentes de ser necesario.
    <div className="App">
      <ReactClass defaultCount={2}/>
      <ReactFunction defaultCount={3}>Componente hijo</ReactFunction>
    </div>
  );
}

export default App;

// Considerar

// - Los props recibidos en un componente no se pueden cambiar, y no deberian nunca ser cambiados, si se requiere alterar su valor deberian 
//   ser almacenados en el estado del componente primero y manipularlos desde ahi
// - No es obligatorio recibir props en un componente
// - Los hooks mas utilizados son "useState" que permite manejar el estado de un componente y "useEffect" que permite ejecutar codigo en base a cambios del estado.
// - Pasar valores de un componente padre a un componente hijo es facil pero al contrario por la naturaleza de los componentes es complicado, para esto se 
//   recurrir al hook useContext (Para crear un contexto que permita compartir informacion) o a la libreria Redux (Que permite crear un estado global).
// - Para controlar formularios se pueden almacenar los valores utilizando muchos useState pero recomiendo utilizar: react-hook-form (https://react-hook-form.com)
//   debido a que cada vez que se actualizar un estado de vuelve a renderizar todo el componentem ReactHookForm permite aislar este comportamiento.
// - Para crear interfaces recomiendo MUI (https://mui.com), el cual da componentes listos para ser utilizados.
// - Es posible importar css propio como al inicio "import './App.css'" pero para agregar clases a un componente no se puede usar "class" sino "className" 
